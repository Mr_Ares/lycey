class Cinema:
    pass


class Hall:
    def __init__(self, rows: int, col: int):
        self.seats = [[Seat(n, s, col * n + s) for s in range(col)] for n in range(rows)]


class Session:
    def __init__(self, name, cost: int, hall: Hall):
        pass


class Seat:
    def __init__(self, row: int, col: int, num: int):
        self.free = True
        self.coordinates = row, col
        self.num = num

    def __str__(self):
        return f'{self.num}   {self.coordinates}'

    def buy(self):
        self.free = False


def main():
    pass


if __name__ == '__main__':
    for i in Hall(5, 5).seats:
        for tt in map(lambda x: str(x), i):
            print(tt)
