PITCHES = ["до", "ре", "ми", "фа", "соль", "ля", "си"]


class Note:
    notes = {'до': 'до-о', 'ре': 'ре-э', 'ми': 'ми-и', 'фа': 'фа-а', 'соль': 'со-оль', 'ля': 'ля-а',
             'си': 'си-и'}

    def __init__(self, note='до', is_long=False):
        if is_long:
            self.note = self.notes[note]
        else:
            self.note = note

    def __str__(self):
        return self.note


class DefaultNote(Note):

    def __str__(self):
        return self.note


class LoudNote(Note):
    def __str__(self):
        return self.note.upper()


class NoteWithOctave(Note):
    def __init__(self, note, octave, is_long=False):
        super(NoteWithOctave, self).__init__(note, is_long)
        self.octave = octave

    def __str__(self):
        return self.note + f' ({self.octave})'


if __name__ == '__main__':
    print(Note("соль"))

    print(LoudNote(PITCHES[4]))
    print(LoudNote("си", is_long=True))

    print(DefaultNote("ми"))
    print(DefaultNote())
    print(DefaultNote(is_long=True))

    print(NoteWithOctave("ре", "первая октава"))
    print(NoteWithOctave("ля", "малая октава", is_long=True))
