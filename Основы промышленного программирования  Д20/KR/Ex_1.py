with open('chocolate.txt', 'r') as f:
    control_word = f.readline().lower()
    read_lines = f.readlines()


out_lines = []
for line in read_lines:
    line_nice_words = []
    line.replace('/n', '')
    line_ls = line.split()
    for word in line_ls:
        is_nice = True
        for lit in word.lower():
            if lit in control_word:
                is_nice = False
                break

        if is_nice:
            line_nice_words.append(word)
    if bool(line_nice_words):
        out_lines.append(line_nice_words)


out_line = '\n'.join([': '.join(line) for line in out_lines])

with open('ticket.txt', 'w') as f:
    f.write(out_line)

