with open(input(), 'r') as f:
    money = int(f.readline())

with open(input(), 'r') as f:
    lines = []
    for line in f.readlines():
        line = line.split()
        lines.append([line[0], int(line[1])])

lines.sort(key=lambda x: x[1], reverse=True)

buy_foods = []
for food in lines:
    if food[1] <= money:
        money -= food[1]
        buy_foods.append(food[0])

out_line = '; '.join(buy_foods) + '\n' + str(money)

with open('goods.txt', 'w') as f:
    f.write(out_line)


