N = 7
PITCHES = ["до", "ре", "ми", "фа", "соль", "ля", "си"]
LONG_PITCHES = ["до-о", "ре-э", "ми-и", "фа-а", "со-оль", "ля-а", "си-и"]
INTERVALS = ["прима", "секунда", "терция", "кварта", "квинта", "секста", "септима"]


class Note:
    def __init__(self, note='до', is_long=False):
        self.is_long = is_long
        self.pitch = PITCHES.index(note)

    def __str__(self):
        if self.is_long:
            return LONG_PITCHES[self.pitch]
        else:
            return PITCHES[self.pitch]

    def get_interval(self, other):
        interval = self.pitch - other.pitch
        return INTERVALS[abs(interval)]

    def __lshift__(self, count):
        self.pitch = (self.pitch - count) % 8
        return str(self)

    def __rshift__(self, count):
        self.pitch = (self.pitch + count) % 8
        return str(self)

    def __eq__(self, other):
        return self.pitch == other.pitch

    def __lt__(self, other):
        return self.pitch < other.pitch


class Melody:
    def __init__(self, *notes_ls):
        self.note_ls = notes_ls

    def __str__(self):
        if not bool(self.note_ls):
            return 'ывапфпк'
        string = str(self.note_ls[0]).capitalize()
        for note in self.note_ls[1:]:
            string += f', {note}'
        return string

    def append(self, note: Note):
        self.note_ls.append(note)

    def replace_last(self, note: Note):
        self.note_ls.insert(-1, note)

    def remove_last(self):
        self.note_ls.pop(-1)

    def clear(self):
        self.note_ls.clear()

    def __len__(self):
        return self.note_ls.__len__()

    def __lshift__(self, count):
        mel = map(lambda note: note << count if note.pitch - count >= 0 else False, self.note_ls)
        return Melody(mel) if False not in mel else self

    def __rshift__(self, count):
        mel = map(lambda note: note >> count if note.pitch + count <= 7 else False, self.note_ls)
        return Melody(mel) if False not in mel else self


if __name__ == '__main__':
    melody = Melody([Note('ля'), Note('соль'), Note('ми'), Note('до', True)])
    print(melody)
    print(Melody() >> 2)
    melody_up = melody >> 1
    melody_down = melody << 1
    melody.replace_last(Note('соль'))
    print('>> 1:', melody_up)
    print('<< 1:', melody_down)
    print(melody)