from functools import total_ordering

N = 7
PITCHES = ["до", "ре", "ми", "фа", "соль", "ля", "си"]
LONG_PITCHES = ["до-о", "ре-э", "ми-и", "фа-а", "со-оль", "ля-а", "си-и"]
INTERVALS = ["прима", "секунда", "терция", "кварта", "квинта", "секста", "септима"]


@total_ordering
class Note:
    def __init__(self, note='до', is_long=False):
        self.is_long = is_long
        self.pitch = PITCHES.index(note)

    def __str__(self):
        return self.stringer(self.pitch)

    def stringer(self, pitch):
        if self.is_long:
            return LONG_PITCHES[pitch]
        else:
            return PITCHES[pitch]

    def get_interval(self, other):
        interval = self.pitch - other.pitch
        return INTERVALS[abs(interval)]

    def __lshift__(self, count):
        pitch = (self.pitch - count) % 7
        return self.stringer(pitch)

    def __rshift__(self, count):
        pitch = (self.pitch + count) % 7
        return self.stringer(pitch)

    def __eq__(self, other):
        return self.pitch == other.pitch

    def __lt__(self, other):
        return self.pitch < other.pitch


if __name__ == '__main__':
    fa2 = Note("фа")
    la = Note("ля", True)
    print(la >> 1)
    print(la >> 2)
    x = fa2 << 4
    print(x)