import csv
import math


def csv_dict_reader(file_obj):
    """
    Read a CSV file using csv.DictReader
    """
    reader = csv.DictReader(file_obj, delimiter=',')
    print(list(reader))


def csv_read():
    with open("schooner_for_sailing.csv") as f_obj:
        reader = csv.DictReader(f_obj, delimiter=';')
    for i in reader:
        print(i)


if __name__ == "__main__":
    print(csv_read())
    exit()
    inp_ls = input().split()
    min_vod, max_matr, count_couts = int(inp_ls[0]), int(inp_ls[1]), math.ceil(int(inp_ls[2]) / 2)
    csv_read()
    