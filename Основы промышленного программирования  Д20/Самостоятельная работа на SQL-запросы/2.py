import sqlite3


def get(bd_table: str, bd_col: str, bd_where: str):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


def insert(bd_table: str, bd_cols: tuple, values: tuple):
    return cur.execute(f'INSERT INTO {bd_table}{bd_cols} VALUES {values}').fetchall()


def update(bd_table: str, new_info, bd_where: str):
    return cur.execute(f'UPDATE {bd_table} SET {new_info} WHERE {bd_where}').fetchall()


def delete(bd_table: str, bd_where: str = ''):
    if bool(bd_where):
        return cur.execute(f'DELETE from {bd_table} WHERE {bd_where}').fetchall()
    else:
        return cur.execute(f'DELETE from {bd_table}').fetchall()


con = sqlite3.connect(input())
cur = con.cursor()

bort, start_damage = input().split()
start_damage = int(start_damage)

side_id = get('Sides', 'id', f'title = "{bort}"')[0][0]
rooms = get('Rooms', 'id, owner, size', f'side_id = {side_id}')
rooms_ids = [elem[0] for elem in rooms]
rooms = {id: [owner, size] for id, owner, size in rooms}
protocols = get('Protocol', 'room_id, broken_thing, damage, can_be_repaired',
                f"room_id IN {tuple(rooms_ids)} AND damage >= {start_damage}")
protocols.sort(key=lambda x: x[2], reverse=True)
out = ''
for room_id, broken_thing, damage, can_be_repaired in protocols:
    owner, size = rooms[room_id]
    out += f'{broken_thing};{damage};{can_be_repaired};{owner};{size}\n'
print(out)

with open('damage.csv', 'w') as file:
    file.write(out)

