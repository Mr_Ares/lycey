with open('input.txt', 'r', encoding='UTF-8') as file:
    strings = list(map(lambda x: x[:-1].lower() if x[-1:] == '\n' else x.lower(), file.readlines()))

count = 0
trigger_word = 'далек'
for string in strings:
    for word in string.split():
        if trigger_word in word:
            if word.index(trigger_word) == 0:
                count += 1
                break

print(count)
