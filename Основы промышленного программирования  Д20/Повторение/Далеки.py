with open('input.txt', 'r', encoding='UTF-8') as file:
    strings = list(map(lambda x: x[:-1].lower() if x[-1:] == '\n' else x.lower(), file.readlines()))

count = 0
trigger_word = 'далек'
for string in strings:
    if trigger_word in string:
        count += 1

print(count)
