things = ['камень', 'ножницы', 'бумага']
ans = ['первый', 'второй', 'ничья']
inp_ls = [things.index(input()) for _ in range(2)]
if inp_ls[0] == inp_ls[1]:
    print(ans[2])
elif (inp_ls[0] + 1) % 3 == inp_ls[1]:
    print(ans[0])
else:
    print(ans[1])
