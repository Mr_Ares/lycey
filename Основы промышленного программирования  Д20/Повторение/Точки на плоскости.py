osi = []
chasti = [0, 0, 0, 0]
for _ in range(int(input())):
    x, y = map(lambda f: int(f), input().split())
    if x == 0 or y == 0:
        print(f'({x}, {y})')
    else:
        if x > 0 and y > 0:
            chasti[0] += 1
        elif x < 0 and y > 0:
            chasti[1] += 1
        elif x < 0 and y < 0:
            chasti[2] += 1
        else:
            chasti[3] += 1

print('I: {}, II: {}, III: {}, IV: {}.'.format(*chasti))
