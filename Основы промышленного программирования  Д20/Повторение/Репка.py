# from Повторение.Tester.Tester import my_print as print
# from Повторение.Tester.Tester import my_input as input
# from Повторение.Tester.Tester import start, test

# start()

out_2 = '{} -> {}'
out_3 = '{} -> {} -> {}'

poryadok = input().split(' -> ')
ans_ls = [input() for _ in range(int(input()))]
for pers in ans_ls:
    if pers == poryadok[0]:
        print(out_2.format(pers, poryadok[1]))
    elif pers == poryadok[-1]:
        print(out_2.format(poryadok[-2], poryadok[-1]))
    else:
        ind = poryadok.index(pers)
        print(out_3.format(poryadok[ind - 1], poryadok[ind], poryadok[ind + 1]))

# test()
