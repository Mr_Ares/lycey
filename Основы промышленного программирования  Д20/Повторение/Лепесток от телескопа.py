with open('input.txt', 'r', encoding='UTF-8') as file:
    test_words = list(map(lambda x: x[:-1] if x[-1:] == '\n' else x, file.readlines()))

word = test_words.pop(0)
words = test_words.copy()
word_letters = [letter for letter in word]
for test_word in test_words:
    letters = word_letters.copy()
    for letter in test_word:
        if letter in letters:
            letters.remove(letter)
        else:
            words.remove(test_word)
            break


print(words.__len__())
print(*words, sep='\n')
