with open('input.txt', 'r', encoding='UTF-8') as file:
    strings = list(map(lambda x: x[:-1].lower() if x[-1:] == '\n' else x.lower(), file.readlines()))

count = 0
trigger_words = ['далек', 'далека', 'далеку', 'далека', 'далеком', 'далеке', 'далеки', 'далеков',
                 'далекам', 'далеков', 'далеками', 'далеках']
for string in strings:
    for word in string.split():
        if word in trigger_words:
            count += 1
            break

print(count)
