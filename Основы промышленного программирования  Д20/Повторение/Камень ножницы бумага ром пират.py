terms = {'ножницы': ['бумага', 'ром'],
         'бумага': ['пират', 'камень'],
         'камень': ['ножницы', 'ром'],
         'ром': ['пират', 'бумага'],
         'пират': ['ножницы', 'камень']}
ans = ['первый', 'второй', 'ничья']
player_1 = input()
player_2 = input()

if player_1 == player_2:
    print(ans[2])
elif player_1 in terms[player_2]:
    print(ans[1])
elif player_2 in terms[player_1]:
    print(ans[0])
else:
    print('ОШИБКА')
