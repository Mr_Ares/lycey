hours = sorted([int(i) for i in input().split()])
minutes = sorted([int(i) for i in input().split()])

times = []

for hour in hours:
    sum_num_hour = hour
    if hour >= 10:
        sum_num_hour = hour // 10 + hour % 10

    for minute in minutes:
        sum_num_minute = minute
        if minute >= 10:
            sum_num_minute = minute // 10 + minute % 10

        if sum_num_hour == sum_num_minute:
            continue
        times.append([str(hour), str(minute)])

for hour, minute in times:
    if len(hour) == 1:
        hour = '0' + hour
    if len(minute) == 1:
        minute = '0' + minute
    print(f'{hour}:{minute}')
