import os

tests = []
main_test_num = 0
main_inputs = []
main_outs = []
for i in os.listdir('Tester'):
    if i in ('Tester.py', '__pycache__'):
        continue
    file = open('Tester/' + i, 'r', encoding='UTF-8')
    inp = []

    while True:
        line = file.readline()
        if line == '\n':
            break
        inp.append(line[:-1])
    out = file.readlines()
    tests.append([inp, out])
    file.close()

input_count = -1


def my_input():
    """

    String:return:
    """
    global input_count
    input_count += 1
    return main_inputs[input_count]




outs = []


def my_print(*args, sep=' ', end='\n'):
    global outs
    outs.append(sep.join(args) + end)


def start(test_num=0):
    global main_test_num
    global main_inputs
    global main_outs
    main_test_num = test_num
    main_inputs = tests[main_test_num][0]
    main_outs = tests[main_test_num][1]


def test():
    if outs == main_outs:
        print('OK')
    else:
        print('ERROR')
        print('Ваш вывод:')
        print(''.join(outs))
        print('Правильный вывод вывод:')
        print(''.join(main_outs))
