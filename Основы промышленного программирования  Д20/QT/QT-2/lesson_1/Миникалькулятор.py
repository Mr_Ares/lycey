# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.15.1
#


import sys

from PyQt5.QtWidgets import QApplication, QMainWindow

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(610, 210)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                           QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(11)
        sizePolicy.setVerticalStretch(11)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 581, 181))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout_11.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.horizontalLayout_11.setContentsMargins(6, 6, 6, 6)
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout()
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.verticalLayout_10 = QtWidgets.QVBoxLayout()
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.label_13 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_13.setObjectName("label_13")
        self.verticalLayout_10.addWidget(self.label_13)
        self.input_int_1 = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.input_int_1.setObjectName("input_int_1")
        self.verticalLayout_10.addWidget(self.input_int_1)
        self.verticalLayout_9.addLayout(self.verticalLayout_10)
        self.verticalLayout_11 = QtWidgets.QVBoxLayout()
        self.verticalLayout_11.setObjectName("verticalLayout_11")
        self.label_14 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_14.setObjectName("label_14")
        self.verticalLayout_11.addWidget(self.label_14)
        self.input_num_2 = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.input_num_2.setObjectName("input_num_2")
        self.verticalLayout_11.addWidget(self.input_num_2)
        self.verticalLayout_9.addLayout(self.verticalLayout_11)
        self.horizontalLayout_11.addLayout(self.verticalLayout_9)
        self.button = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                           QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.button.sizePolicy().hasHeightForWidth())
        self.button.setSizePolicy(sizePolicy)
        self.button.setObjectName("button")
        self.horizontalLayout_11.addWidget(self.button)
        self.verticalLayout_12 = QtWidgets.QVBoxLayout()
        self.verticalLayout_12.setObjectName("verticalLayout_12")
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.label_15 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_15.setObjectName("label_15")
        self.horizontalLayout_12.addWidget(self.label_15)
        self.lcdNumber_sum = QtWidgets.QLCDNumber(self.horizontalLayoutWidget)
        self.lcdNumber_sum.setObjectName("lcdNumber_sum")
        self.horizontalLayout_12.addWidget(self.lcdNumber_sum)
        self.verticalLayout_12.addLayout(self.horizontalLayout_12)
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.label_16 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_16.setObjectName("label_16")
        self.horizontalLayout_13.addWidget(self.label_16)
        self.lcdNumber_diff = QtWidgets.QLCDNumber(self.horizontalLayoutWidget)
        self.lcdNumber_diff.setObjectName("lcdNumber_diff")
        self.horizontalLayout_13.addWidget(self.lcdNumber_diff)
        self.verticalLayout_12.addLayout(self.horizontalLayout_13)
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.label_17 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_17.setObjectName("label_17")
        self.horizontalLayout_14.addWidget(self.label_17)
        self.lcdNumber_comp = QtWidgets.QLCDNumber(self.horizontalLayoutWidget)
        self.lcdNumber_comp.setObjectName("lcdNumber_comp")
        self.horizontalLayout_14.addWidget(self.lcdNumber_comp)
        self.verticalLayout_12.addLayout(self.horizontalLayout_14)
        self.horizontalLayout_15 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_15.setObjectName("horizontalLayout_15")
        self.label_18 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_18.setObjectName("label_18")
        self.horizontalLayout_15.addWidget(self.label_18)
        self.lcdNumber_quot = QtWidgets.QLCDNumber(self.horizontalLayoutWidget)
        self.lcdNumber_quot.setObjectName("lcdNumber_quot")
        self.horizontalLayout_15.addWidget(self.lcdNumber_quot)
        self.verticalLayout_12.addLayout(self.horizontalLayout_15)
        self.horizontalLayout_11.addLayout(self.verticalLayout_12)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_13.setText(_translate("MainWindow", "Первое число(целое):"))
        self.label_14.setText(_translate("MainWindow", "Второе число(целое):"))
        self.button.setText(_translate("MainWindow", "=>"))
        self.label_15.setText(_translate("MainWindow", "Сумма:"))
        self.label_16.setText(_translate("MainWindow", "Разность:"))
        self.label_17.setText(_translate("MainWindow", "Произведение:"))
        self.label_18.setText(_translate("MainWindow", "Частное:"))


class MyWidget(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        # Вызываем метод для загрузки интерфейса из класса Ui_MainWindow,
        # остальное без изменений
        self.setupUi(self)
        self.button.clicked.connect(self.run)

    def run(self):
        num_1 = int(self.input_int_1.text())
        num_2 = int(self.input_num_2.text())
        lcds = [self.lcdNumber_sum, self.lcdNumber_diff, self.lcdNumber_comp, self.lcdNumber_quot]
        summ = num_1 + num_2
        diff = num_1 - num_2
        comp = num_1 * num_2
        if num_2 != 0:
            quot = num_1 / num_2
        else:
            quot = 'Error'

        for i, num in enumerate([summ, diff, comp, quot]):
            lcds[i].display(num)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec_())
