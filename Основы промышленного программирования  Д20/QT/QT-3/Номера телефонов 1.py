def normalize_call_number(call_number: str):
    out_num = ''
    was_started_number = False
    for i, num in enumerate(call_number):
        if (num == '+' or num == '8') and not was_started_number:
            was_started_number = True
        if was_started_number and num in '+0123456789':
            out_num += num
    return out_num


def replace_8_to_plus_7(call_number: str):
    if call_number[0] == '8':
        call_number = '+7' + call_number[1:]

    return call_number


def call_number_is_normal(call_number: str):
    condition_1 = ('+7' in call_number or '8' in call_number)
    condition_2 = '--' not in call_number and call_number[-1] != '-'
    condition_3 = ((call_number.count('(') == 1 and call_number.count(')') == 1)
                   or ((call_number.count('(') == 0 and
                        call_number.count(')') == 0)))
    return condition_1 and condition_2 and condition_3


def chek_out_long(call_number: str):
    return len(call_number) == 12


class Number_Format_Error(Exception):
    pass


class Call_Number_Long_Error(Exception):
    pass


def main():
    inp = input()
    try:
        if not call_number_is_normal(inp):
            raise Number_Format_Error
        normal_call_number = replace_8_to_plus_7(normalize_call_number(inp))
        if not chek_out_long(normal_call_number):
            raise Call_Number_Long_Error
        print(normal_call_number)

    except Number_Format_Error:
        print('неверный формат')
    except Call_Number_Long_Error:
        print('неверное количество цифр')


if __name__ == '__main__':
    main()
