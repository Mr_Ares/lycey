class PasswordError(Exception):
    pass


class LengthError(PasswordError):
    pass


class LetterError(PasswordError):
    pass


class DigitError(PasswordError):
    pass


class SequenceError(PasswordError):
    pass


def check_password(password: str):
    keyboard = '1234567890qwertyuiopasdfghjklzxcvbnmйцукенгшщзхъфывапролджэячсмитьбю'
    numbers = '1234567890'
    if password.__len__() < 9:
        raise LengthError
    elif password.islower() or password.isupper():
        raise LetterError
    elif len(list(set(password) & set(numbers))) == 0:
        raise DigitError
    triple = password[:2]
    for i in password[2:]:
        triple = triple[1:] + i
        if triple.lower() in keyboard:
            raise SequenceError