class PasswordError(Exception):
    pass


class LengthError(PasswordError):
    pass


class LetterError(PasswordError):
    pass


class DigitError(PasswordError):
    pass


class SequenceError(PasswordError):
    pass


def check_password(password: str):
    keyboard = '1234567890_qwertyuiop_asdfghjkl_zxcvbnm_йцукенгшщзхъ_фывапролджэ_ячсмитьбю'
    numbers = '1234567890'
    if password.__len__() < 9:
        raise LengthError
    elif password.islower() or password.isupper() or password.isdigit():
        raise LetterError
    elif len(list(set(password) & set(numbers))) == 0:
        raise DigitError
    triple = password[:3]
    if triple.lower() in keyboard:
        raise SequenceError
    for i in password[3:]:
        triple = triple[1:] + i
        if triple.lower() in keyboard:
            raise SequenceError


if __name__ == '__main__':
    try:
        check_password(input())
        print('ok')
    except PasswordError as error:
        print('error')
