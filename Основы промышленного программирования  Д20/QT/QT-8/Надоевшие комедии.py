import sqlite3


def get(bd_table: str, bd_col: str, bd_where: str):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


def insert(bd_table: str, bd_cols: tuple, values: tuple):
    return cur.execute(f'INSERT INTO {bd_table}{bd_cols} VALUES {values}').fetchall()


def update(bd_table: str, new_info, bd_where: str):
    return cur.execute(f'UPDATE {bd_table} SET {new_info} WHERE {bd_where}').fetchall()


def delete(bd_table: str, bd_where: str = ''):
    if bool(bd_where):
        return cur.execute(f'DELETE from {bd_table} WHERE {bd_where}').fetchall()
    else:
        return cur.execute(f'DELETE from {bd_table}').fetchall()


def get_result(name: str):
    con = sqlite3.connect(name)
    global cur
    cur = con.cursor()
    comedy_id = get('genres', 'id', "title = 'комедия'")[0][0]
    delete('films', f'genre = {comedy_id}')
    con.commit()
    con.close()


if __name__ == '__main__':
    get_result('db.sqlite')
