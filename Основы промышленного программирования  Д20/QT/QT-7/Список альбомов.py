import sqlite3

i_am_program = False
if i_am_program:
    genre = 'Rock'
else:
    genre = input()

con = sqlite3.connect('music_db.sqlite')
cur = con.cursor()


def get(bd_table, bd_col, bd_where):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


genre_id = get('Genre', 'Genreid', f"Name = '{genre}'")[0][0]
album_ids = list(map(lambda x: x[0], get('Track', 'DISTINCT AlbumId', f"GenreId = '{genre_id}'")))
ls = get('Album', 'DISTINCT ArtistId, Title', f"AlbumId IN {tuple(album_ids)}")
out_dc = {}
for id in map(lambda x: x[0], ls):
    out_dc[str(id)] = []

for id, alb in ls:
    out_dc[str(id)].append(alb)
for key, item in out_dc.items():
    item.sort()
    print(*item, sep='\n')
