import sqlite3


i_am_programing = True
if i_am_programing:
    db_name = 'db.sqlite'
else:
    db_name = input()


con = sqlite3.connect(db_name)
cur = con.cursor()


def get(bd_table, bd_col, bd_where):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


janr_id = get('genres', 'id', "title = 'детектив'")[0][0]

for elem in get('films', 'title', f"genre = {janr_id} AND year <= 2000 AND year >= 1995"):
    print(elem[0])

con.close()