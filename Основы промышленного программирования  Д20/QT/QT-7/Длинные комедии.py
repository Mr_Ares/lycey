import sqlite3


i_am_programing = False
if i_am_programing:
    db_name = 'db.sqlite'
else:
    db_name = input()


con = sqlite3.connect(db_name)
cur = con.cursor()


def get(bd_table, bd_col, bd_where):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


comedy_id = get('genres', 'id', "title = 'комедия'")[0][0]

for elem in get('films', 'title', f"genre = {comedy_id} AND duration >= 60"):
    print(elem[0])

con.close()