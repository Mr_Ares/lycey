import sqlite3

i_am_program = False
if i_am_program:
    db_name = 'db.sqlite'
else:
    db_name = input()

con = sqlite3.connect(db_name)
cur = con.cursor()


def get(bd_col, bd_table, bd_where):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


st = [elem[0] for elem in get('year', 'films', "title LIKE 'Х%'")]
was = []
for y in st:
    if y not in was:
        print(y)
        was.append(y)

con.close()
