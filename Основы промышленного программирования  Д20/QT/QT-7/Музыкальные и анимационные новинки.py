import sqlite3

db_name = input()

con = sqlite3.connect(db_name)

# Создание курсора
cur = con.cursor()

# Выполнение запроса и получение всех результатов
genre = cur.execute("""SELECT id FROM genres
 WHERE title = 'музыка' OR title = 'анимация'""").fetchall()
genre = genre[0][0], genre[1][0]
result = cur.execute(f'SELECT title FROM films WHERE year >= 1997 AND genre IN {genre}').fetchall()
# Вывод результатов на экран
for elem in result:
    print(elem[0])

con.close()
