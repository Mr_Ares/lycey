import sqlite3

i_am_program = False
if i_am_program:
    genre = 'Rock'
else:
    genre = input()

con = sqlite3.connect('../music_db.sqlite')
cur = con.cursor()


def get(bd_table, bd_col, bd_where):
    return cur.execute(f'SELECT {bd_col} FROM {bd_table} WHERE {bd_where}').fetchall()


genre_id = get('Genre', 'Genreid', f"Name = '{genre}'")[0][0]
album_ids = list(map(lambda x: x[0], get('Track', 'DISTINCT AlbumId', f"GenreId = '{genre_id}'")))
artists_ids = list(map(lambda x: x[0], get('Album', 'DISTINCT ArtistId',
                                           f"AlbumId IN {tuple(album_ids)}")))
artists_name = list(map(lambda x: x[0], get('Artist', 'Name', f"ArtistId IN {tuple(artists_ids)}")))
artists_name.sort()
print(*artists_name, sep='\n')
