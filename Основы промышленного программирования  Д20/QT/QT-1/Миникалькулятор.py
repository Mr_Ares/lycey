import sys

from PyQt5.QtWidgets import *


class ConvertWidget(QWidget):
    def __init__(self):
        super(ConvertWidget, self).__init__()

        self.setWindowTitle('Фокус со словами')
        self.is_Right = True

        self.main_layout = QHBoxLayout(self)
        self.input_layout = QVBoxLayout(self)
        self.output_layout = QVBoxLayout(self)

        self.input_layout_1 = QVBoxLayout(self)
        self.input_value_1 = QLineEdit(self)
        self.input_value_1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.input_layout_1.addWidget()

        self.input_layout_2 = QVBoxLayout(self)
        self.input_value_1 = QLineEdit(self)
        self.input_value_1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)


        self.button = QPushButton(self)
        self.button.setText('=>')
        self.button.clicked.connect(self.convert)
        self.button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.output_value = QLineEdit(self)

        self.output_value.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.main_layout.addWidget(self.input_value)
        self.main_layout.addWidget(self.button)
        self.main_layout.addWidget(self.output_value)

        self.setLayout(self.main_layout)

    def init_input(self):
        self.input_layout_1 = QVBoxLayout(self)
        self.input_value_1 = QLineEdit(self)
        self.input_value_1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.input_layout_1.addWidget()

        self.input_layout_2 = QVBoxLayout(self)
        self.input_value_1 = QLineEdit(self)
        self.input_value_1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def convert(self):
        text_1 = self.input_value.text()
        text_2 = self.output_value.text()
        self.input_value.setText(text_2)
        self.output_value.setText(text_1)

        if self.is_Right:
            self.is_Right = False
            self.button.setText('<=')
        else:
            self.is_Right = True
            self.button.setText('=>')





if __name__ == '__main__':
    app = QApplication(sys.argv)
    wnd = ConvertWidget()
    wnd.show()
    sys.exit(app.exec())
