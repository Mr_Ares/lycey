import sys

from PyQt5.QtWidgets import *


class ConvertWidget(QWidget):
    def __init__(self):
        super(ConvertWidget, self).__init__()

        self.setWindowTitle(__file__)
        self.is_Right = True

        self.main_layout = QHBoxLayout(self)

        self.input_value = QLineEdit(self)
        self.input_value.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)


        self.convert_button = QPushButton(self)
        self.convert_button.setText('=>')
        self.convert_button.clicked.connect(self.convert)
        self.convert_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.output_value = QLineEdit(self)

        self.output_value.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.main_layout.addWidget(self.input_value)
        self.main_layout.addWidget(self.convert_button)
        self.main_layout.addWidget(self.output_value)

        self.setLayout(self.main_layout)

    def convert(self):
        text_1 = self.input_value.text()
        text_1 = eval(text_1)
        self.output_value.setText(str(text_1))




if __name__ == '__main__':
    app = QApplication(sys.argv)
    wnd = ConvertWidget()
    wnd.show()
    sys.exit(app.exec())
