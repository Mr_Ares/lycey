with open('prices.txt', 'r', encoding='UTF-8') as file:
    file_lines = file.readlines()

if bool(file_lines):
    su = 0
    for line in file_lines:
        name, col, cost = line.split('\t')
        su += float(col) * float(cost)

    print(su)
else:
    print(0)
