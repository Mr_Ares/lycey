def palindrome():
    with open('input.dat', 'rb') as file:
        file_bytes = file.read()
    return is_palindrome(file_bytes)


def is_palindrome(file_bytes):
    if file_bytes.__len__() % 2 == 0:
        return file_bytes[:len(file_bytes) // 2] == file_bytes[:(len(file_bytes) // 2) - 1:-1]
    else:
        return file_bytes[:len(file_bytes) // 2] == file_bytes[:(len(file_bytes) // 2):-1]
