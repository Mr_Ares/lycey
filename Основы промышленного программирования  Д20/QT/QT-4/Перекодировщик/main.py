table = {"й": "j", "ц": "c", "у": "u", "к": "k", "е": "e", "н": "n",
         "г": "g", "ш": "sh", "щ": "shh", "з": "z", "х": "h", "ъ": "#",
         "ф": "f", "ы": "y", "в": "v", "а": "a", "п": "p", "р": "r",
         "о": "o", "л": "l", "д": "d", "ж": "zh", "э": "je", "я": "ya",
         "ч": "ch", "с": "s", "м": "m", "и": "i", "т": "t", "ь": "'",
         "б": "b", "ю": "ju", "ё": "jo"}


def big_letter_trans(letter):
    return table[letter.lower()].capitalize()


def lower_letter_trans(letter):
    return table[letter]


with open('cyrillic.txt', 'r', encoding='UTF-8') as file:
    text = file.read()

out_text = ''

for i in text:
    print(i)
    if i.lower() in table.keys():
        if i.isupper():
            out_text += big_letter_trans(i)
        else:
            out_text += lower_letter_trans(i)
    else:
        out_text += i

with open('transliteration.txt', 'w') as file:
    file.write(out_text)
