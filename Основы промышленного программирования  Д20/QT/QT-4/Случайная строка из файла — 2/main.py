from random import randint

with open('lines.txt', 'r') as file:
    lines = file.readlines()

if bool(lines):
    lines_count = len(lines)
    # print('Кол-во линий:', lines_count)
    rand_line_num = randint(0, lines_count - 1)
    # print('Номер строки:', rand_line_num)
    rand_line = lines[rand_line_num]
    print(rand_line)