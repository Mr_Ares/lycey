with open('pipes.txt', 'r', encoding='UTF-8') as file:
    lines = file.read().split('\n')


def write(out, file_name):
    with open(file_name, 'w') as file:
        file.write(out)


times = []
for num, line in enumerate(lines):
    if line == ' ':
        break
    else:
        times.append(float(line) * 60)

work_pipes_nums = [int(x) for x in lines[num + 1].split()]
work_pipes = [times[s - 1] for s in work_pipes_nums]

U_summ = 0
for time in work_pipes:
    U_summ += 1 / time

write(str(1 / U_summ), 'time.txt')

