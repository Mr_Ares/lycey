with open('input.txt', 'r', encoding='UTF-8') as file:
    lines = file.readlines()

units = {'B': 1024 ** 0, 'KB': 1024 ** 1, 'MB': 1024 ** 2, 'GB': 1024 ** 3, 'TB': 1024 ** 4}

files = {}

for line in lines:
    name, size, unit = line.split()
    ext = name.split('.')[1]
    size = int(size) * units[unit]
    if ext not in files.keys():
        files[ext] = [[name, size]]
    else:
        files[ext].append([name, size])


def change_size_unit(in_size: int):
    for out_unit, unit_size in units.items():
        if in_size / unit_size >= 1 and in_size / unit_size / 1024 < 1:
            return f'{round(in_size / unit_size)} {out_unit}'


out = ''
for key in sorted(files.keys()):
    one_ext_files = files[key]
    out_names = sorted([x[0] for x in one_ext_files])
    out_size = sum([x[1] for x in one_ext_files])
    out += '\n'.join(out_names) + '\n'
    out += '----------\n'
    out += 'Summary: ' + change_size_unit(out_size) + '\n\n'

with open('output.txt', 'w') as file:
    file.write(out)
