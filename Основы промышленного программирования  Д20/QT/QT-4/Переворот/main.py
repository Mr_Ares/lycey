def reverse():
    with open('input.dat', 'rb') as file:
        reverse_bytes = file.read()[::-1]

    with open('output.dat', 'wb') as file:
        file.write(reverse_bytes)
