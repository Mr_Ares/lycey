import sys

ignored_errors = ['W']

excluded_files = ['check.py']

line_length = 101

style_guide = flake8.get_style_guide(ignore=ignored_errors,

                                     excluded=excluded_files,

                                     max_line_length=line_length)

report = style_guide.check_files('solution.py')

if report.get_statistics('E') != []:
    print('Код не соответствует стандарту PEP8')

    sys.exit(1)
s = ['output.txt', '5d4816c0-9c68-48d0-80cd-516651087fcf', 'input.txt', 'executingScript',
 'scoring.json', '215afc50-d52f-4bd3-9a3d-02ea0de4a485_out', 'compilingScript', 'build.sh',
 'Makefile', 'edd95734-8ea4-45de-baaf-0e6393d96f99', 'check.py', 'solution.py',
 '215afc50-d52f-4bd3-9a3d-02ea0de4a485_err']
