from zipfile import ZipFile

with ZipFile('input.zip') as my_zip:
    name_list = my_zip.namelist()

main_dir = {}
structure = []


def open_dir(op_dire: dict, se_dire: list):
    if bool(se_dire):
        name = se_dire.pop(0)
        if name != '':
            if name in op_dire.keys():
                open_dir(op_dire[name], se_dire)
            else:
                if bool(se_dire):
                    op_dire[name] = {}
                else:
                    op_dire[name] = None
                open_dir(op_dire[name], se_dire)


for name in name_list:
    item_split = name.split('/')
    open_dir(main_dir, item_split)


def out(dir: dict, vlog: int):
    for i in dir.keys():
        print('  ' * vlog + i)
        if bool(dir[i]):
            out(dir[i], vlog + 1)


out(main_dir, 0)
