import os


def get_files_sizes():
    def human_read_format(size: int):
        units = {'Б': 1024 ** 0, 'КБ': 1024 ** 1, 'МБ': 1024 ** 2, 'ГБ': 1024 ** 3, 'ТБ': 1024 ** 4}
        for out_unit, unit_size in units.items():
            if size / unit_size >= 1 and size / unit_size / 1024 < 1:
                return f'{round(size / unit_size)}{out_unit}'
            elif size == 0:
                return '0Б'

    out_str = ''
    for item in os.listdir():
        if os.path.isfile(item):
            out_str += f'{item} {human_read_format(os.path.getsize(item))}\n'
    return out_str


print(get_files_sizes())
