# from pydantic import BaseModel
import json


# class Scoring(BaseModel):
#     required_pretests: list
#     required_tests: list[int]
#     outcome: int
#     points: int
#
#     def coff(self):
#         return self.points / len(self.required_tests)
#
#
# class Scores(BaseModel):
#     scoring: list[Scoring]
#
#
# tests = Scores.parse_file('scoring.json').scoring

def coff(n: int):
    t = tests[n]
    return t['points'] / len(t['required_tests'])


with open('scoring.json') as file:
    tests = json.load(file)['scoring']

with open('input.txt') as file:
    ans = [n + 1 for n, i in enumerate(file.read().split('\n')) if i == 'ok']

out = 0
for n, test in enumerate(tests):
    out += len(set(ans) & set(test['required_tests'])) * coff(n)
print(int(out))
