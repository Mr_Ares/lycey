import requests


def geocoder_request(adr):
    return requests.get("http://geocode-maps.yandex.ru/1.x/"
                        f"?apikey=40d1649f-0493-4b70-98ba-98533de7710b&geocode={adr}&format=json")


for city in 'Хабаровск, Уфа, Нижний Новгород, Калининград'.split(', '):
    response = geocoder_request(city)
    if response:
        json_response = response.json()
        toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
        province = toponym['metaDataProperty']['GeocoderMetaData']['Address']['Components'][1][
            'name']
        print(city, '-', province)
    else:
        print("Ошибка выполнения запроса:")
        print(geocoder_request)
        print("Http статус:", response.status_code, "(", response.reason, ")")
