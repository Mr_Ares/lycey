import requests
from pprint import pprint


geocoder_request = "http://geocode-maps.yandex.ru/1.x/" \
                   "?apikey=40d1649f-0493-4b70-98ba-98533de7710b&" \
                   "geocode=Москва, Красная пл-дь, 1&format=json"

response = requests.get(geocoder_request)
if response:
    json_response = response.json()
    # pprint(json_response)
    toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
    print(toponym["metaDataProperty"]["GeocoderMetaData"]["text"])
    print(toponym["Point"]["pos"])

else:
    print("Ошибка выполнения запроса:")
    print(geocoder_request)
    print("Http статус:", response.status_code, "(", response.reason, ")")
