from pprint import pprint

import requests


def geocoder_request(adr):
    return requests.get("http://geocode-maps.yandex.ru/1.x/"
                        f"?apikey=40d1649f-0493-4b70-98ba-98533de7710b&geocode={adr}&format=json")


min_cord = 200
true_city = ''
for city in input('Введите города:').split(', '):
    response = geocoder_request(city)
    if response:
        json_response = response.json()
        toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
        cord = float(toponym['Point']['pos'].split()[1])
        if cord < min_cord:
            min_cord = cord
            true_city = city
    else:
        print("Ошибка выполнения запроса:")
        print(geocoder_request)
        print("Http статус:", response.status_code, "(", response.reason, ")")

print(true_city)
