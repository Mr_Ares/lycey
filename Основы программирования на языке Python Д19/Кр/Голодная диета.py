file = open('input.txt', 'r').readlines()
# file[-1] = file[-1] + 'n'
inp = map(lambda x: [x.split(' - ')[0], x.split(' - ')[1][:-1]], file)
dc = {}

for eat, eater in inp:
    if eater in dc.keys():
        dc[eater].append(eat)
    else:
        dc[eater] = [eat]

for eater in dc.keys():
    eat = set(dc[eater])
    print(f'{eater}:', end=' ')
    print(*eat, sep=', ')
