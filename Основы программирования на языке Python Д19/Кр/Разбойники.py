class Highwayman:
    def __init__(self, name, property, count):
        self.name = name
        self.property = property
        self.count = count
        self.bl = False

    def __str__(self):
        return f'Highwayman {self.name} - {self.property}'

    def lie(self):
        if self.bl:
            self.count += 1
            self.bl = False
            return True
        else:
            self.bl = True
            return False

    def boast(self):
        return 'Uhaha' * self.count

    def change_property(self, property):
        self.property = property

    def __gt__(self, other):
        if self.count > other.count:
            return True
        elif self.count < other.count:
            return False

        if len(self.property) > len(other.property):
            return True
        elif len(self.property) < len(other.property):
            return False

        if len(self.name) > len(other.name):
            return True
        elif len(self.name) < len(other.name):
            return False

        ls = [self.name, other.name]
        ls.sort()
        if ls[0] == other.name:
            return True
        else:
            return False

    def __lt__(self, other):
        if other.count > self.count:
            return True
        elif other.count < self.count:
            return False

        if len(other.property) > len(self.property):
            return True
        elif len(other.property) < len(self.property):
            return False

        if len(other.name) > len(self.name):
            return True
        elif len(other.name) < len(self.name):
            return False

        ls = [other.name, self.name]
        ls.sort()
        if ls[0] == self.name:
            return True
        else:
            return False

    def __eq__(self, other):
        if other.count != self.count:
            return False
        if len(other.name) != len(self.name):
            return False
        if len(other.property) != len(self.property):
            return False
        if other.name != self.name:
            return False
        return True

    def __ne__(self, other):
        if other.count != self.count:
            return True
        if len(other.name) != len(self.name):
            return True
        if len(other.property) != len(self.property):
            return True
        if other.name != self.name:
            return True
        return False

    def __le__(self, other):
        if other.count == self.count and len(other.name) == len(self.name) and len(
                other.property) == len(self.property) and other.name == self.name:
            return True
        if self.count > other.count:
            return True
        elif self.count < other.count:
            return False

        if len(self.property) > len(other.property):
            return True
        elif len(self.property) < len(other.property):
            return False

        if len(self.name) > len(other.name):
            return True
        elif len(self.name) < len(other.name):
            return False

        ls = [self.name, other.name]
        ls.sort()
        if ls[0] == self.name:
            return True
        else:
            return False

    def __ge__(self, other):
        if other.count == self.count and len(other.name) == len(self.name) and len(
                other.property) == len(self.property) and other.name == self.name:
            return True
        if other.count > self.count:
            return True
        elif other.count < self.count:
            return False

        if len(other.property) > len(self.property):
            return True
        elif len(other.property) < len(self.property):
            return False

        if len(other.name) > len(self.name):
            return True
        elif len(other.name) < len(self.name):
            return False

        ls = [other.name, self.name]
        ls.sort()
        if ls[0] == other.name:
            return True
        else:
            return False
