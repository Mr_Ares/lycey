class Transport:
    pass


class Earth(Transport):
    pass


class No_Earth(Transport):
    pass


class Alien(No_Earth):
    pass


class Moon_Rover(Alien):
    pass


class Mars_Rover(Alien):
    pass


class Space_Transport(No_Earth):
    pass


class Space_Ship(Space_Transport):
    pass


class Air(Earth):
    pass


class Water(Earth):
    pass


class Ground(Earth):
    pass


class Road(Ground):
    pass


class Plane(Air):
    pass


class Helicopter(Air):
    pass


class Drone(Air):
    pass


class Freighter(Water):
    pass


class Catamaran(Water):
    pass


class Ship(Water):
    pass


class Speedboat(Water):
    pass


class Submarine(Water):
    pass


class Yacht(Water):
    pass


class Ferry(Water):
    pass


class Raft(Water):
    pass


class Hovercraft(Water):
    pass


class Motorboat(Water):
    pass


class Car(Road):
    pass


class Bus(Road):
    pass


class Motorbike(Road):
    pass


class Tram(Road):
    pass


class Truck(Road):
    pass
