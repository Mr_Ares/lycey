class BaseObject:
    def __init__(self, x, y, z):
        self.cord = x, y, z

    def get_coordinates(self):
        return self.cord


class Block(BaseObject):
    def shatter(self):
        self.cord = None, None, None


class Entity(BaseObject):
    def move(self, x, y, z):
        self.cord = x, y, z


class Thing(BaseObject):
    pass
