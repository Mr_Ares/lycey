def check_password(password):
    def decorator(func):
        def decorated_func(*args, **kwargs):
            if input('Введите пароль:') != password:
                print('Отказано в доступе')
                return None
            return func(*args, **kwargs)
        return decorated_func
    return decorator


@check_password('password')
def make_burger(typeOfMeat, withOnion=False, withTomato=True):
    pass
