memory = {}


def cached(function):

    def wrapper(*args):
        if args not in memory:
            memory[args] = function(*args)
        return memory[args]
    return wrapper


@cached
def fib(n):
    num1 = num2 = 1
    if n == 0:
        return 0
    elif 1 < n <= 2:
        return 1
    else:
        for i in range(2, n):
            num1, num2 = num2, num1 + num2
        return num2


print(fib(100))
print(fib(25))
print(fib(50))
print(memory)
