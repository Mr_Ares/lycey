original_print = print


def print(*args, **kwargs):
    original_print(*(x.upper() for x in args), **kwargs)