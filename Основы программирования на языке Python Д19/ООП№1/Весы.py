class Balance:
    def __init__(self):
        self.right = 0
        self.left = 0

    def add_right(self, add):
        self.right += add

    def add_left(self, add):
        self.left += add

    def result(self):
        res = '='
        if self.right > self.left:
            res = 'R'
        elif self.right < self.left:
            res = 'L'
        return res
