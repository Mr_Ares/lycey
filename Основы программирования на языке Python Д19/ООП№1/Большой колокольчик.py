class BigBell:
    def __init__(self):
        self.ding = True

    def sound(self):
        if self.ding:
            self.ding = False
            print('ding')
        else:
            self.ding = True
            print('dong')
