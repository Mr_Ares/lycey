class TicTacToeBoard:
    def __init__(self):
        self.board = [['-' for _ in range(3)] for _ in range(3)]

    def get_field(self):
        return self.board
