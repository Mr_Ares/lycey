class BoundingRectangle:
    def __init__(self):
        self.points = []

    def add_point(self, x, y):
        self.points.append((x, y))

    def srt_points_x(self):
        return sorted(self.points, key=lambda x: x[0])

    def srt_points_y(self):
        return sorted(self.points, key=lambda x: x[1])

    def width(self):
        x = self.srt_points_x()
        x1 = x[0][0]
        x2 = x[-1][0]
        if x1 < 0 and x2 < 0:
            x1 *= -1
            x2 *= -1
            return x1 - x2
        elif x1 >= 0 and x2 >= 0:
            return x2 - x1
        elif x1 <= 0 and x2 >= 0:
            x1 *= -1
            return x1 + x2

    def height(self):
        x = self.srt_points_y()
        x1 = x[0][1]
        x2 = x[-1][1]
        if x1 <= 0 and x2 <= 0:
            x1 *= -1
            x2 *= -1
            return x1 - x2
        elif x1 >= 0 and x2 >= 0:
            return x2 - x1
        elif x1 <= 0 and x2 >= 0:
            x1 *= -1
            return x1 + x2

    def bottom_y(self):
        x = self.srt_points_y()
        return x[0][1]

    def top_y(self):
        x = self.srt_points_y()
        return x[-1][1]

    def left_x(self):
        x = self.srt_points_x()
        return x[0][0]

    def right_x(self):
        x = self.srt_points_x()
        return x[-1][0]
