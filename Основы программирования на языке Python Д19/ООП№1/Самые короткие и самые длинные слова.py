class MinMaxWordFinder:
    def __init__(self):
        self.words = []

    def add_sentence(self, word):
        self.words.extend(word.split())

    def shortest_words(self):
        words = self.words
        return list(sorted(filter(lambda x: min(len(i) for i in words) == len(x), words)))

    def longest_words(self):
        words = set(self.words)
        return sorted(list(filter(lambda x: len(max(words)) == len(x), words)))
