class Button:
    def __init__(self):
        self.click_coun = 0

    def click(self):
        self.click_coun += 1

    def click_count(self):
        return str(self.click_coun)

    def reset(self):
        self.click_coun = 0
