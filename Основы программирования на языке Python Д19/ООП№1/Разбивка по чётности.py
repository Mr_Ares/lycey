class OddEvenSeparator:
    def __init__(self):
        self.numbers = []

    def add_number(self, numb):
        self.numbers.append(numb)

    def odd(self):
        return filter(lambda x: x % 2, self.numbers)

    def even(self):
        return filter(lambda x: x % 2 == 0, self.numbers)
