from itertools import product

nominals = [i for i in range(2, 11)] + ['валет', 'дама', 'король', 'туз']
suits = ['пик', 'треф', 'бубен', 'червей']
del_suit = input()
del suits[suits.index(del_suit)]
for el in product(nominals, suits):
    print(*el)
