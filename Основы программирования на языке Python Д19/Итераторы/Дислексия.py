from itertools import permutations

dct = input().lower().split()
message = input().lower().split()
res = []
for el in message:
    if any(''.join(word) in dct and ''.join(word) != el for word in permutations(el, len(el))):
        res.append('#' * len(el))
    else:
        res.append(el)
print(' '.join(res))
