from functools import reduce
from sys import stdin

data = list(map(str.strip, stdin))
res = reduce(lambda x, y: min(x, y), data)
print(res)
