from sys import stdin

data = list(map(str.strip, stdin))
name_of_books = data[0].split('\t')
name_of_best_shop, *prices = \
    min(data[1:], key=lambda x: sum(int(i) for i in x.split('\t')[1:])).split('\t')
print(name_of_best_shop)
for el in zip(name_of_books, prices):
    print('\t'.join(el))
