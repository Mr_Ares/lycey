from math import gcd
from functools import reduce
from sys import stdin

numbers = [int(i) for i in map(str.strip, stdin)]
print(reduce(gcd, numbers))
