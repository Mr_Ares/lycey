from sys import stdin

data = [el for el in map(str.strip, stdin)]
sum1 = sum(int(x.split()[0]) for x in data)
sum2 = sum(int(x.split()[0]) for x in set(data))
print(sum1 - sum2)
print(1154635 // 100000)