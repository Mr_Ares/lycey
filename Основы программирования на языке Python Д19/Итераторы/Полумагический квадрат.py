from sys import stdin

data = list(map(str.strip, stdin))
matrix = [[int(i) for i in row.split()] for row in data]
all_rows_is_equal = all(sum(row) == sum(matrix[0]) for row in matrix[1:])
all_cols_is_equal = all(sum(matrix[0]) == sum(col) for col in zip(*matrix))
print('YES' if all_rows_is_equal & all_cols_is_equal else 'NO')
