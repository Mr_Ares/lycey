from sys import stdin

dct = {}
for el in map(str.strip, stdin):
    capital, country, population = el.split()
    key = str(int(population) // 100000) + '00 - ' + str(int(population) // 100000 + 1) + '00' \
        if int(population) >= 100000 else '0 - 100'
    if key not in dct.keys():
        dct[key] = [capital]
    else:
        dct[key].append(capital)

sort_keys = sorted(dct.keys(), key=lambda key: int(key.split(' - ')[0]))
for key in sort_keys:
    print(key + ':', ', '.join(sorted(dct[key])))
