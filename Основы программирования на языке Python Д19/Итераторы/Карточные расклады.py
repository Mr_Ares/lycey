from itertools import product, combinations

nominals = [str(i) for i in range(2, 11)] + ['валет', 'дама', 'король', 'туз']
suits = sorted(['пик', 'треф', 'бубен', 'червей'])
all_cards = product(nominals, suits)  # создание всех возможных карт
triple_combinations = combinations(all_cards, 3)  # создание всех возможных тройных комбинаций карт

# отбор подходящих комбинаций с помощью функции any(), затем сортировка полученных комбинаций
# по названию карты с помощью lambda-функции и в конце концов представление комбинаций карт в
# виде строки с помощью join() и занесение их в генератор res
res = (', '.join([card[0] + ' ' + card[1] for card in sorted(combination, key=lambda x: x[0])])
       for combination in triple_combinations
       if any(card[0] in ['валет', 'дама', 'король', 'туз']
              for card in combination) and any(card[1] == 'червей' for card in combination))
print(*sorted(res), sep='\n')  # вывод отсортированных комбинаций на экран
