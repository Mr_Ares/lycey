from sys import stdin

words = (word for expr in map(str.strip, stdin) for word in expr.split())
words_without_punctuation = map(lambda x: x if x[-1].isalpha() else x[:-1], words)
res = ([index, word] for index, word in enumerate(words_without_punctuation) if word.istitle())
sort_res = sorted(res, key=lambda x: x[1])
tmp = []
for el in sort_res:
    if el[1] not in tmp:
        print(el[0], '-', el[1])
        tmp.append(el[1])
