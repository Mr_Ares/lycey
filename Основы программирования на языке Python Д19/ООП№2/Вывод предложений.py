class Paragraph:
    def __init__(self, wt):
        self.wt = wt
        self.ls = []

    def add_word(self, word):
        self.ls.append(word)

    def set_lines(self):
        ct = self.wt
        line = ''
        bl = True
        ls = []
        for word in self.ls:
            if len(line) == 0:
                line += word
                bl = True
            elif len(line) + 1 + len(word) <= ct:
                line += ' ' + word
                bl = True
            else:
                ls.append(line)
                line = word
                bl = False
        if bl or len(line.split()) == 1:
            ls.append(line)
        self.ls = []
        return ls


class LeftParagraph(Paragraph):
    def end(self):
        for i in self.set_lines():
            print(i)


class RightParagraph(Paragraph):
    def end(self):
        for i in self.set_lines():
            print((self.wt - len(i)) * ' ' + i)
