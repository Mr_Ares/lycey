class Selector:
    def __init__(self, values):
        self.values = values[::]

    def get_odds(self):
        return list(filter(lambda x: x % 2 == 1, self.values))

    def get_evens(self):
        return list(filter(lambda x: x % 2 == 0, self.values))

