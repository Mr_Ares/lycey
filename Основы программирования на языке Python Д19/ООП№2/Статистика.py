class Stat:
    def __init__(self):
        self.ls = []

    def add_number(self, numb):
        self.ls.append(numb)


class MinStat(Stat):
    def result(self):
        if bool(self.ls):
            return min(self.ls)
        else:
            return None


class MaxStat(Stat):
    def result(self):
        if bool(self.ls):
            return max(self.ls)
        else:
            return None


class AverageStat(Stat):
    def result(self):
        if bool(self.ls):
            return sum(self.ls) / len(self.ls)
        else:
            return None
