class Table:
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.tab = [[0 for _ in range(cols)] for _ in range(rows)]

    def get_value(self, row, col):
        if self.rows > row >= 0 and self.cols > col >= 0:
            return self.tab[row][col]
        else:
            return None

    def set_value(self, row, col, value):
        self.tab[row][col] = value

    def n_rows(self):
        return self.rows

    def n_cols(self):
        return self.cols
