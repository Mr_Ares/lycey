class Table:
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.tab = [[0 for _ in range(cols)] for _ in range(rows)]

    def get_value(self, row, col):
        if self.tab.__len__() > row >= 0 and self.tab[0].__len__() > col >= 0:
            return self.tab[row][col]
        else:
            return None

    def set_value(self, row, col, value):
        self.tab[row][col] = value

    def n_rows(self):
        return len(self.tab)

    def n_cols(self):
        return len(self.tab[0])

    def delete_row(self, row):
        self.tab.pop(row)

    def delete_col(self, col):
        for i in self.tab.__len__():
            self.tab[i].pop(col)

    def add_row(self, row):
        self.tab.insert(row, [0 for i in range(self.n_cols())])

    def add_col(self, col):
        for i in range(self.n_rows()):
            self.tab[i].insert(0, col)

tab = Table(2, 2)

for i in range(tab.n_rows()):
    for j in range(tab.n_cols()):
        print(tab.get_value(i, j), end=' ')
    print()
print()

tab.set_value(0, 0, 10)
tab.set_value(0, 1, 20)
tab.set_value(1, 0, 30)
tab.set_value(1, 1, 40)

for i in range(tab.n_rows()):
    for j in range(tab.n_cols()):
        print(tab.get_value(i, j), end=' ')
    print()
print()

for i in range(-1, tab.n_rows() + 1):
    for j in range(-1, tab.n_cols() + 1):
        print(tab.get_value(i, j), end=' ')
    print()
print()

tab.add_row(0)
tab.add_col(1)

for i in range(-1, tab.n_rows() + 1):
    for j in range(-1, tab.n_cols() + 1):
        print(tab.get_value(i, j), end=' ')
    print()
print()