class Date:
    def __init__(self, g, m, d):
        self.date = {'g': str(g), 'm': str(m), 'd': str(d)}

    def set_year(self, g):
        self.date['g'] = str(g)

    def set_month(self, m):
        self.date['m'] = str(m)

    def set_day(self, d):
        self.date['d'] = str(d)

    def get_year(self):
        return self.date['g']

    def get_month(self):
        return self.date['m']

    def get_day(self):
        return self.date['d']

    def normal(self):
        dc = {'g': self.date['g'], 'm': self.date['m'], 'd': self.date['d']}
        if self.date['d'].__len__() == 1:
            dc['d'] = '0' + self.date['d']
        if self.date['m'].__len__() == 1:
            dc['m'] = '0' + self.date['m']
        return dc


class AmericanDate(Date):
    def format(self):
        dd = Date.normal(self)
        return dd['m'] + '.' + dd['d'] + '.' + dd['g']


class EuropeanDate(Date):
    def format(self):
        dd = Date.normal(self)
        return dd['d'] + '.' + dd['m'] + '.' + dd['g']
