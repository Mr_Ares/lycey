class Point:
    def __init__(self, x, y):
        self.cord = x, y

    def __eq__(self, other):
        if self.cord == other.cord:
            return True
        else:
            return False

    def __ne__(self, other):
        if self.cord != other.cord:
            return True
        else:
            return False
