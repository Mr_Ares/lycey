class Queue:
    def __init__(self, *r):
        self.a = list(r)

    def append(self, *values):
        self.a += values

    def copy(self):
        return Queue(*self.a)

    def pop(self):
        return self.a.pop(0)

    def extend(self, queue):
        self.a += queue.a

    def next(self):
        return Queue(*self.a[1:])

    def __add__(self, other):
        return Queue(*(self.a + other.a))

    def __iadd__(self, other):
        self.a += other.a
        return self

    def __eq__(self, other):
        return self.a == other.a

    def __rshift__(self, other):
        if other > len(self.a):
            return Queue()
        return Queue(*self.a[other:])

    def __str__(self):
        return f'[{" -> ".join(list(map(lambda x: str(x), self.a[::])))}]'

    def __next__(self):
        return Queue(*self.a[1:])
