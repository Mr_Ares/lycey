class FoodInfo:
    def __init__(self, proteins, fats, carbohydrates):
        self.carbohydrates = carbohydrates
        self.fats = fats
        self.proteins = proteins

    def get_proteins(self):
        return self.proteins

    def get_fats(self):
        return self.fats

    def get_carbohydrates(self):
        return self.carbohydrates

    def get_kcalories(self):
        return self.proteins * 4 + self.fats * 9 + self.carbohydrates * 4

    def __add__(self, other):
        return FoodInfo(self.proteins + other.proteins, self.fats + other.fats,
                        self.carbohydrates + other.carbohydrates)

