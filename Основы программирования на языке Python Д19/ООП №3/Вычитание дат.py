import datetime as dt


class Date:
    def __init__(self, m, d):
        self.date = dt.date(year=2001, month=m, day=d)

    def __sub__(self, other):
        days = self.date - other.date
        return days.days
