class Polynomial:
    def __init__(self, coefficients):
        self.coefficients = coefficients

    def __call__(self, x):
        ret = self.coefficients[0]
        for step, coefficient in enumerate(self.coefficients[1:]):
            ret += coefficient * (x ** (step + 1))
        return ret

    def __add__(self, other):
        a = self.coefficients.copy()
        b = other.coefficients.copy()
        minlen = min(len(a), len(b))
        return Polynomial([a[i] + b[i] for i in range(minlen)] + a[minlen:] + b[minlen:])

