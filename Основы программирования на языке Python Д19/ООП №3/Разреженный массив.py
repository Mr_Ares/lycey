class SparseArray:
    def __init__(self):
        self.dc = {}

    def __getitem__(self, item):
        if item in self.dc.keys():
            return self.dc[item]
        else:
            return 0

    def __setitem__(self, key, value):
        self.dc[key] = value
