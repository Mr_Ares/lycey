class ReversedList:
    def __init__(self, ls):
        self.ls = list(reversed(ls))

    def __len__(self):
        return len(self.ls)

    def __getitem__(self, item):
        return self.ls[item]