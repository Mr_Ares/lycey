class SquareFunction:
    def __init__(self, a, b, c):
        self.dc = {'a': a, 'b': b, 'c': c}

    def __call__(self, x):
        return self.dc['a'] * (x ** 2) + self.dc['b'] * x + self.dc['c']

