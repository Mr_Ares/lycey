def eratosthenes(N):
    array_of_integers = [i for i in range(3, N + 1)]
    i = 2
    while len(array_of_integers) != 0:
        for j in array_of_integers:
            if j % i == 0:
                print(array_of_integers[array_of_integers.index(j)], end=' ')
                del array_of_integers[array_of_integers.index(j)]

        i = array_of_integers[0]
        del array_of_integers[0]