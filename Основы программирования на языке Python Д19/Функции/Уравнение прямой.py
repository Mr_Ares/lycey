def equation(xy1, xy2):
    x1 = float(xy1.split(";")[0])
    y1 = float(xy1.split(";")[1])
    x2 = float(xy2.split(";")[0])
    y2 = float(xy2.split(";")[1])
    if x1 == x2:
        print(x1)
    elif y1 == y2:
        print(y2)
    else:
        k = (y1 - y2) / (x1 - x2)
        b = y2 - k * x2
        print(k, b)
