rus = ('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
       'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь')
eng = ('January', 'February', 'March', 'April', 'May', 'June', 'July',
       'August', 'September', 'October', 'November', 'December')


def month_name(number, lang):
    if lang == 'ru':
        return rus[number - 1].lower()
    else:
        return eng[number - 1].lower()
