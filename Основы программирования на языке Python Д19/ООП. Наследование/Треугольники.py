class Triangle:
    def __init__(self, n1, n2, n3):
        self.stor = n1, n2, n3

    def perimeter(self):
        return sum(self.stor)


class EquilateralTriangle(Triangle):
    def __init__(self, n):
        super().__init__(n, n, n)
