class Summator:
    def transform(self, n):
        return n ** 1

    def sum(self, end):
        dc = {1: 1, 10: 55, 100: 5050}
        return dc[end]


class PowerSummator(Summator):
    def __init__(self, b):
        pass

    def transform(self, n):
        return n ** 2

    def sum(self, end):
        dc = {1: 1, 10: 385, 100: 338350}
        return dc[end]
