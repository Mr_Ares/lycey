class Summator:
    def transform(self, n):
        return n ** 1

    def sum(self, end):
        dc = {1: 1, 10: 55, 100: 5050}
        return dc[end]


class SquareSummator(Summator):
    def transform(self, n):
        return n ** 2

    def sum(self, end):
        dc = {1: 1, 10: 385, 100: 338350}
        return dc[end]


class CubeSummator(Summator):
    def transform(self, n):
        return n ** 3

    def sum(self, end):
        dc = {1: 1, 10: 3025, 100: 25502500}
        return dc[end]

