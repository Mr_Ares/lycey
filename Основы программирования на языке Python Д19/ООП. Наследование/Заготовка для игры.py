from math import sqrt


class Weapon:
    def __init__(self, name, damage, range):
        self.name = name
        self.damage = damage
        self.range = range

    def hit(self, actor, target):
        x1, y1 = actor.get_coords()
        x2, y2 = target.get_coords()
        if target.is_alive():
            if sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2) <= self.range:
                target.get_damage(self.damage)
                print(f'Врагу нанесен урон оружием {self.__str__()} в размере {self.damage}')
            else:
                print(f'Враг слишком далеко для оружия {self.__str__()}')
        else:
            print('Враг уже повержен')

    def __str__(self):
        return self.name


class BaseCharacter:
    def __init__(self, pos_x, pos_y, hp):
        self.pos = pos_x, pos_y
        self.hp = hp

    def move(self, delta_x, delta_y):
        self.pos = self.pos[0] + delta_x, self.pos[1] + delta_y

    def is_alive(self):
        if self.hp >= 0:
            return True
        else:
            return False

    def get_damage(self, amount):
        self.hp -= amount

    def get_coords(self):
        return self.pos


class BaseEnemy(BaseCharacter):
    def __init__(self, pos_x, pos_y, weapon, hp):
        self.weapon = weapon
        super().__init__(pos_x, pos_y, hp)

    def hit(self, target):
        if type(target) != MainHero:
            print('Могу ударить только Главного героя')
        else:
            self.weapon.hit(self, target)

    def __str__(self):
        pos_x, pos_y = self.pos
        return f'Враг на позиции ({pos_x}, {pos_y}) с оружием {str(self.weapon)}'


class MainHero(BaseCharacter):
    def __init__(self, pos_x, pos_y, name, hp):
        super().__init__(pos_x, pos_y, hp)
        self.name = name
        self.weapon = None
        self.weapons = []

    def hit(self, target):
        if self.weapon is None:
            print('Я безоружен')
        else:
            if type(target) == BaseEnemy:
                self.weapon.hit(self, target)
            else:
                print('Могу ударить только Врага')

    def add_weapon(self, weapon):
        if type(weapon) == Weapon:
            if bool(self.weapons):
                self.weapons.append(weapon)
            else:
                self.weapons.append(weapon)
                self.weapon = weapon
            print(f'Подобрал {str(weapon)}')
        else:
            print('Это не оружие')

    def next_weapon(self):
        if bool(self.weapons):
            if self.weapons.__len__() == 1:
                print('У меня только одно оружие')
            else:
                self.weapon = self.weapons[(self.weapons.index(self.weapon) + 1) % self.weapons.__len__()]
                print(f'Сменил оружие на {self.weapon}')
        else:
            print('Я безоружен')

    def heal(self, amount):
        self.hp += amount
        if self.hp > 200:
            self.hp = 200
        print(f'Полечился, теперь здоровья {self.hp}')
