class Profile:
    def __init__(self, name):
        self.name = name

    def info(self):
        return ''

    def describe(self):
        print(self.name, self.info())


class Vacancy(Profile):
    def __init__(self, name, money):
        self.money = money
        super().__init__(name)

    def info(self):
        return f'Предлагаемая зарплата: {self.money}'


class Resume(Profile):
    def __init__(self, name, stag):
        self.stag = stag
        super().__init__(name)

    def info(self):
        return f'Стаж работы: {self.stag}'
