class Chessman:
    can_move_to = ()
    charr = ''

    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color

    def set_position(self, row, col):
        self.row = row
        self.col = col

    def get_color(self):
        return self.color

    def char(self):
        return self.charr

    def can_move(self, row, col):
        if 0 <= row < 8 and 0 <= col < 8:
            for mr, mc in self.can_move_to:
                if self.row + mr == row and self.col + mc == col:
                    return True
        return False


class Queen(Chessman):
    charr = 'Q'
    can_move_to = [[i, 0] for i in range(-7, 8)] + [[0, i] for i in range(-7, 8)] + \
                  [[i, i] for i in range(-7, 8)] + [[-i, i] for i in range(-7, 8)]
