BLACK = 2
WHITE = 3


class Chessman:
    can_move_to = ()
    charr = ''

    def __init__(self, row, col, color):
        self.row = row
        self.col = col
        self.color = color

    def set_position(self, row, col):
        self.row = row
        self.col = col

    def get_color(self):
        return self.color

    def char(self):
        return self.charr

    def can_move(self, row, col):
        if 0 <= row < 8 and 0 <= col < 8:
            for mr, mc in self.can_move_to:
                if self.row + mr == row and self.col + mc == col:
                    return True
        return False


class Knight(Chessman):
    can_move_to = ([1, -2], [2, -1], [2, 1], [1, 2], [-1, 2], [-2, 1], [-2, -1], [-1, -2])
    charr = 'N'


class Bishop(Chessman):
    can_move_to = [[i, i] for i in range(1, 8)] + [[-i, i] for i in range(1, 8)] + \
                  [[i, -i] for i in range(1, 8)] + [[-i, -i] for i in range(1, 8)]
    charr = 'B'


class Queen(Chessman):
    charr = 'Q'
    can_move_to = [[i, 0] for i in range(-7, 8)] + [[0, i] for i in range(-7, 8)] + \
                  [[i, i] for i in range(-7, 8)] + [[-i, i] for i in range(-7, 8)]


class Pawn(Chessman):
    charr = 'P'

    def __init__(self, row, col, color):
        super().__init__(row, col, color)
        self.can_move_to = [[1, 0], [2, 0]] if self.color == WHITE else [[-1, 0], [-2, 0]]


class Rook(Chessman):
    charr = 'R'
    can_move_to = [[i, 0] for i in range(-7, 8)] + [[0, i] for i in range(-7, 8)]


def correct_coords(row, col):
    return 0 <= row < 8 and 0 <= col < 8


def opponent(color):
    if color == WHITE:
        return BLACK
    else:
        return WHITE


class Board:
    def __init__(self):
        self.color = WHITE
        self.field = []

    def current_player_color(self):
        return self.color

    def cell(self, row, col):
        piece = self.field[row][col]
        if piece is None:
            return '  '
        color = piece.get_color()
        c = 'w' if color == WHITE else 'b'
        return c + piece.char()

    def move_piece(self, row, col, row1, col1):
        if not correct_coords(row, col) or not correct_coords(row1, col1):
            return False
        if row == row1 and col == col1:
            return False  # нельзя пойти в ту же клетку
        piece = self.field[row][col]
        if piece is None:
            return False
        if piece.get_color() != self.color:
            return False
        if not piece.can_move(row1, col1):
            return False
        self.field[row][col] = None  # Снять фигуру.
        self.field[row1][col1] = piece  # Поставить на новое место.
        piece.set_position(row1, col1)
        self.color = opponent(self.color)
        return True

    def is_under_attack(self, row, col, color):
        field = [i for o in self.field for i in o if i is not None if i.get_color() == color]
        for i in field:
            if i.can_move(row, col):
                return True
        return False
