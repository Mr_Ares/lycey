debug_mode = False
'''
позволяет не вводить команды каждый раз, а ввести в файл Tester.txt,
откуда они буду вводиться автоматически
'''

if debug_mode:
    with open('Tester.txt', 'r', encoding='utf-8') as file:
        lines = file.readlines()


    def input(arg):
        print(arg, end='')
        if bool(lines):
            line = lines.pop(0)
            print(line, end='')
            return line
        else:
            print('Тест пройден!')
            return 'exit'


class User:
    """Класс пользователя"""

    def __init__(self, login, password):
        self.login = login
        self.password = password


class Message:  # Класс сообщения
    """Класс сообщения"""

    def __init__(self, sender, receiver, text):
        self.sender = sender
        self.receiver = receiver
        self.text = text


class MailClient:  # класс Клиента
    """Клинтская программа с помощью которой пользователь обращается к серверу"""

    def __init__(self, name):
        self.name = name
        self.server = None
        self.user = None

    def registration(self, server, login, password):

        """Регистрация пользователя"""

        server.add_user(User(login, password))
        self.server = server
        self.user = server.users[login]
        print('Вы успешно зарегестрировались!')

    def auth(self, server, login, password):

        """Авторизация пользователя"""

        if self.server is None and self.user is None:
            if login in server.users.keys():
                if password == server.users[login].password:
                    self.server = server
                    self.user = server.users[login]
                    print(f'Вы вошли в аккаунт {login}.')
                else:
                    print('Неверный пароль!')
            else:
                print('Данный логин не зарегестрирован на этом сервере!!!')
        else:
            print('Вы не вышли из аккаунта!')

    def log_out(self):

        """Выход и учётки пользователя"""

        self.server = None
        self.user = None

    def send_mail(self, server, user, message):

        """Отправка сообщения"""

        server.mail.append(Message(self.user, user, message))

    def receive_mail(self):

        """Получение сообщений"""

        messages = []
        for n, i in enumerate(self.server.mail):
            if i.receiver == self.user.login:
                messages.append(self.server.mail.pop(n))
        return messages


class MailServer:
    """Почтовый сервер"""

    def __init__(self, name):
        self.name = name
        self.users = {}
        self.mail = []

    def add_user(self, user):
        """Добавление новых пользователей"""

        self.users[user.login] = user


def main():
    """Основная функция программы"""

    servers = {}
    clients = {}
    active_client = None
    print('Программа почта!',
          'Есть сервера и есть клиенты, пользователи авторизуются с помощью клиентской программы'
          'на сервереа, а сервера обробатывают запросы клиентов.',
          'Команды:',
          'new_server Server_Name                    -- создать новый сервер',
          'servers                                   -- вывести существующие сервера',
          'new_client Client_Name                    -- создать новый клиент',
          'clients                                   -- вывести существующие клиенты',
          'client                                    -- выбрать клиент',
          'reg Server Login Password                 -- зарегестрироваться на сервере',
          'auth Server Login Password                -- авторизироваться на сервере',
          'log_out                                   -- выйти из учётки ',
          'users Server                              -- показать пользователей на севере',
          'send_mail Server User                     -- отправить сообщение',
          'receive_mail                              -- получить сообщения',
          'exit                                      -- выйти из программы',
          '', '', sep='\n')
    while True:

        """Основной цикл программы"""

        command, *args = input(f'Active client:'  # Ввод команды
                               f' {active_client.name if active_client is not None else None}.'
                               f' Command=>').split()

        """V Исполнение команд V"""

        if command == 'new_server':
            servers[args[0]] = MailServer(args[0])

        elif command == 'servers':
            if bool(servers):
                for n, i in enumerate(servers, 1):
                    print(f'{n}. {i}')
            else:
                print('Нет серверов!')

        elif command == 'new_client':
            clients[args[0]] = MailClient(args[0])
            active_client = clients[args[0]]

        elif command == 'clients':
            if bool(clients):
                for n, i in enumerate(clients, 1):
                    print(f'{n}. {i}')
            else:
                print('Нет клиентов!')

        elif command == 'client':
            active_client = clients[args[0]]

        elif command == 'reg':
            try:
                active_client.registration(servers[args[0]], *args[1:])
            except KeyError:
                print('Неверно указан сервер')

        elif command == 'auth':
            try:
                active_client.auth(servers[args[0]], *args[1:])
            except KeyError:
                print('Неверно указан сервер')

        elif command == 'log_out':
            active_client.log_out()
            print('Вы вышли из аккаунта.')

        elif command == 'send_mail':
            text = input('Сообщение:')
            try:
                active_client.send_mail(servers[args[0]], *args[1:], text)
            except KeyError:
                print('Неверно указан сервер')
        elif command == 'receive_mail':
            messages = active_client.receive_mail()
            if bool(messages):
                print(f'Непрочитанных сообщений -- {len(messages)}')
                for message in messages:
                    print(f'Сообщение от {message.sender.login}.')
                    print(message.text)
            else:
                print('У вас нет сообщений.')

        elif command == 'exit':
            break


if __name__ == '__main__':
    """Запуск программы"""

    main()
