from math import sqrt


class Sqrt_fun:
    def __call__(self, *args, **kwargs):
        ls = []
        for i in args:
            ls.append(sqrt(int(i)))
        if len(ls) == 1:
            return ls[0]
        return ls


class X:
    def __call__(self, *args, **kwargs):
        ls = []
        for i in args:
            ls.append(i)
        if len(ls) == 1:
            return ls[0]
        return ls


dc_funs = {'sqrt_fun': Sqrt_fun(), 'x': X()}


class Define:
    def __init__(self, args):
        funs = list(dc_funs.keys())
        x = None
        one = args[0]
        two = args[2]
        if args[2] in funs:
            two = f'dc_funs["{args[2]}"](x)'
        if args[0] in funs:
            one = f'dc_funs["{args[0]}"](x)'
        fun = 'self.fun = ' + 'lambda x: ' + one + args[1] + two
        exec(fun)

    def __call__(self, *args, **kwargs):
        ls = []
        for i in args:
            ls.append(self.fun(int(i)))
        if len(ls) == 1:
            return ls[0]
        return ls


def main():
    ans_ls = [input().split() for _ in range(int(input()))]
    for command, name, *args in ans_ls:
        if command == 'define':
            dc_funs[name] = Define(args)
        elif command == 'calculate':
            ls = dc_funs[name](*args)
            print(*ls)


if __name__ == '__main__':
    main()
