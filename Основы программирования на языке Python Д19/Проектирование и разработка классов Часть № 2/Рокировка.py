WHITE, BLACK = 1, 2


def opponent(color):
    return BLACK if color == WHITE else WHITE


class Piece:
    def __init__(self, color):
        self.color = color

    def get_color(self):
        return self.color


class Pawn(Piece):
    def char(self):
        return 'P'

    def can_move(self, board, row, col, row1, col1):
        # Пешка может ходить только по вертикали
        # "взятие на проходе" не реализовано
        if col != col1:
            return False

        if board.field[row1][col1] is not None:
            return False

        # Пешка может сделать из начального положения ход на 2 клетки
        # вперёд, поэтому поместим индекс начального ряда в start_row.
        if self.color == WHITE:
            direction = 1
            start_row = 1
        else:
            direction = -1
            start_row = 6

        # ход на 1 клетку
        if row + direction == row1:
            return True

        # ход на 2 клетки из начального положения
        if (row == start_row
                and row + 2 * direction == row1
                and board.field[row + direction][col] is None):
            return True
        return False

    def can_attack(self, board, row, col, row1, col1):
        if board.field[row1][col1] is None or board.field[row][col] is None:
            return False
        if board.field[row][col].get_color() == board.field[row1][col1].get_color():
            return False
        direction = 1 if (self.color == WHITE) else -1
        return (row + direction == row1
                and (col + 1 == col1 or col - 1 == col1))


class Knight(Piece):
    def char(self):
        return 'N'

    def can_move(self, board, row, col, row1, col1):
        return True if abs(col - col1) * abs(row - row1) == 2 \
                       and row != row1 and col != col1 else False


class Bishop(Piece):
    def char(self):
        return 'B'

    def can_move(self, board, row, col, row1, col1):
        if abs(row1 - row) == abs(col1 - col):
            if row - col == row1 - col1:
                step = 1 if (row1 >= row) else -1
                for r in range(row + step, row1, step):
                    c = col - row + r
                    if not (board.get_piece(r, c) is None):
                        return False
                return True
            if row + col == row1 + col1:
                step = 1 if (row1 >= row) else -1
                for r in range(row + step, row1, step):
                    c = row + col - r
                    if not (board.get_piece(r, c) is None):
                        return False
                return True
        return False


class Queen(Piece):
    def char(self):
        return 'Q'

    def can_move(self, board, row, col, row1, col1):
        if not correct_coords(row1, col1):
            return False
        piece1 = board.get_piece(row1, col1)
        if not (piece1 is None) and piece1.get_color() == self.color:
            return False
        if row == row1 or col == col1:
            step = 1 if (row1 >= row) else -1
            for r in range(row + step, row1, step):
                if not (board.get_piece(r, col) is None):
                    return False
            step = 1 if (col1 >= col) else -1
            for c in range(col + step, col1, step):
                if not (board.get_piece(row, c) is None):
                    return False
            return True
        if row - col == row1 - col1:
            step = 1 if (row1 >= row) else -1
            for r in range(row + step, row1, step):
                c = col - row + r
                if not (board.get_piece(r, c) is None):
                    return False
            return True
        if row + col == row1 + col1:
            step = 1 if (row1 >= row) else -1
            for r in range(row + step, row1, step):
                c = row + col - r
                if not (board.get_piece(r, c) is None):
                    return False
            return True
        return False


class Rook(Piece):
    def __init__(self, color):
        super().__init__(color)
        self.is_moving = False

    def char(self):
        return 'R'

    def can_move(self, board, row, col, row1, col1):
        # Невозможно сделать ход в клетку,
        # которая не лежит в том же ряду или столбце клеток.
        if row != row1 and col != col1:
            return False

        step = 1 if (row1 >= row) else -1
        for r in range(row + step, row1, step):
            # Если на пути по вертикали есть фигура
            if not (board.get_piece(r, col) is None):
                return False

        step = 1 if (col1 >= col) else -1
        for c in range(col + step, col1, step):
            # Если на пути по горизонтали есть фигура
            if not (board.get_piece(row, c) is None):
                return False
        return True

    def can_attack(self, board, row, col, row1, col1):
        return self.can_move(board, row, col, row1, col1)


def correct_coords(row, col):
    return 0 <= row < 8 and 0 <= col < 8


class King(Piece):
    def __init__(self, color):
        super().__init__(color)
        self.is_moving = False

    def char(self):
        return 'K'

    def can_move(self, board, row, col, row1, col1):  # if board.is_under_attack(row1, col1,
        # opponent(self.color)):   return False ==> старый теперь нерабочий способ
        # надо реализовать проверку короля мб клетка будет под боем (хз как) ???
        if abs(row - row1) <= 1 and abs(col - col1) <= 1:
            return True
        return False


class Board:
    def __init__(self):
        self.color = WHITE
        self.field = []

    def current_player_color(self):
        return self.color

    def cell(self, row, col):
        piece = self.field[row][col]
        if piece is None:
            return '  '
        color = piece.get_color()
        c = 'w' if color == WHITE else 'b'
        return c + piece.char()

    def is_under_attack(self, row, col, color):
        for i in range(8):
            for j in range(8):
                if self.field[i][j] is not None:
                    piece = self.field[i][j]
                    if piece.can_move(self, i, j, row, col) and piece.get_color() == color:
                        return True
        return False

    def move_piece(self, row, col, row1, col1):
        '''Переместить фигуру из точки (row, col) в точку (row1, col1).
        Если перемещение возможно, метод выполнит его и вернёт True.
        Если нет --- вернёт False'''
        # if piece.get_color() != self.color: return False
        if not correct_coords(row, col) or not correct_coords(row1, col1):
            return False
        if row == row1 and col == col1:
            return False  # нельзя пойти в ту же клетку
        piece = self.field[row][col]
        if piece is None:
            return False
        if self.field[row1][col1] is None:
            if not piece.can_move(self, row, col, row1, col1):
                return False
        elif self.field[row][col].get_color() == self.field[row1][col1].get_color():
            return False
        elif self.field[row1][col1].get_color() == opponent(piece.get_color()):
            if not piece.can_attack(self, row, col, row1, col1):
                return False
        else:
            return False
        if isinstance(piece, Rook) or isinstance(piece, King):
            piece.is_moving = True
        self.field[row][col] = None  # Снять фигуру.
        self.field[row1][col1] = piece  # Поставить на новое место.
        self.color = opponent(self.color)
        return True

    def get_piece(self, row, col):
        return self.field[row][col]

    def move_and_promote_pawn(self, row, col, row1, col1, char):
        piece = self.field[row][col]
        if piece is None:
            return False
        if not isinstance(piece, Pawn):
            return False
        if piece.can_attack(self, row, col, row1, col1) and row1 in [7, 0] \
                or piece.can_move(self, row, col, row1, col1) and row1 in [7, 0]:
            if char == 'Q':
                self.field[row1][col1] = Queen(piece.get_color())
            elif char == 'R':
                self.field[row1][col1] = Rook(piece.get_color())
            elif char == 'N':
                self.field[row1][col1] = Knight(piece.get_color())
            elif char == 'B':
                self.field[row1][col1] = Bishop(piece.get_color())
            self.field[row][col] = None
            return True
        return False

    def castling0(self):
        ind = 0 if self.color == WHITE else 7
        if self.field[ind][4] is None:
            return False
        if self.field[ind][0] is None:
            return False
        if not isinstance(self.field[ind][0], Rook):
            return False
        if not isinstance(self.field[ind][4], King):
            return False
        if self.field[ind][4].is_moving:
            return False
        if self.field[ind][0].is_moving:
            return False
        if any(self.field[ind][i] is not None for i in range(1, 4)):
            return False
        self.field[ind][2] = self.field[ind][4]
        self.field[ind][4] = None
        self.field[ind][3] = self.field[0][0]
        self.field[ind][0] = None
        self.color = opponent(self.color)
        return True

    def castling7(self):
        ind = 0 if self.color == WHITE else 7
        if self.field[ind][4] is None:
            return False
        if self.field[ind][7] is None:
            return False
        if not isinstance(self.field[ind][7], Rook):
            return False
        if not isinstance(self.field[ind][4], King):
            return False
        if self.field[ind][4].is_moving:
            return False
        if self.field[ind][7].is_moving:
            return False
        if any(self.field[ind][i] is not None for i in range(5, 7)):
            return False
        self.field[ind][6] = self.field[ind][4]
        self.field[ind][4] = None
        self.field[ind][5] = self.field[ind][7]
        self.field[ind][7] = None
        self.color = opponent(self.color)
        return True


def print_board(board):  # Распечатать доску в текстовом виде (см. скриншот)
    print('     +----+----+----+----+----+----+----+----+')
    for row in range(7, -1, -1):
        print(' ', row, end='  ')
        for col in range(8):
            print('|', board.cell(row, col), end=' ')
        print('|')
        print('     +----+----+----+----+----+----+----+----+')
    print(end='        ')
    for col in range(8):
        print(col, end='    ')
    print()


def main():
    # Создаём шахматную доску
    board = Board()
    while True:
        # Выводим положение фигур на доске
        print_board(board)
        # Подсказка по командам
        print('Команды:')
        print('    exit                               -- выход')
        print('    move <row> <col> <row1> <col1>     -- ход из клетки (row, col)')
        print('                                          в клетку (row1, col1)')
        # Выводим приглашение игроку нужного цвета
        if board.current_player_color() == WHITE:
            print('Ход белых:')
        else:
            print('Ход черных:')
        command = input()
        if command == 'exit':
            break
        move_type, row, col, row1, col1 = command.split()
        row, col, row1, col1 = int(row), int(col), int(row1), int(col1)
        if board.move_piece(row, col, row1, col1):
            print('Ход успешен')
        else:
            print('Координаты некорректы! Попробуйте другой ход!')
