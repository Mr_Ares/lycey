import datetime as dt


class Conference:
    def __init__(self, name):
        self.name = name
        self.conference = []

    def __str__(self):
        return self.name

    def get_reports(self):
        return [report for report in self.conference if type(report) == Report]

    def get_breaks(self):
        return [breaks for breaks in self.conference if type(breaks) == Break]

    def add_report(self, report):
        if len(self.get_reports()) == 0:
            self.conference.append(report)
        else:
            if report.start_time > self.get_reports()[-1].end_time:
                self.conference.append(Break(self.get_reports()[-1].end_time, report.start_time))
                self.conference.append(report)
            else:
                print(f'Доклад не может быть начат'
                      f' раньше{self.get_reports()[-1].end_time.strftime("%H:%M")}')

    def get_start_time(self):
        return self.get_reports()[0].start_time

    def get_end_time(self):
        return self.get_reports()[-1].end_time

    def get_all_reports_time(self):
        return sum([report.deltatime.seconds for report in self.get_reports()])

    def get_all_breaks_time(self):
        return sum([breaak.deltatime.seconds for breaak in self.get_breaks()])

    def print_info(self):
        print(f'Название:                           {self.name}')
        print(f'Начало конференции:                 {self.get_start_time()}')
        print(f'Конец конференции:                  {self.get_end_time()}')
        print(f'Общее время докладов:               {self.get_all_reports_time() // 60} минут')
        print(f'Общее время перерывов:              {self.get_all_breaks_time() // 60} минут')
        print('Программа:')
        for event in self.conference:
            if type(event) == Report:
                print(f'Доклад: {str(event)}, продолжительность {event.deltatime}')
            else:
                print(f'Перерыв {event.deltatime}')


class Report:
    def __init__(self):
        self.name = input('Введите название доклада:')
        self.date = [int(i) for i in input('Введите дату доклада(YYYY.MM.DD):').split('.')]
        start_time = [int(i) for i in input('Введите время начала доклада(HH:MM):').split(':')]
        duration = [int(i) for i in input('Введите продолжительность доклада(HH:MM):').split(':')]
        self.start_time = dt.datetime(*self.date, hour=int(start_time[0]),
                                      minute=int(start_time[1]))
        self.deltatime = dt.timedelta(hours=int(duration[0]), minutes=int(duration[1]))
        self.end_time = self.start_time + self.deltatime

    def __str__(self):
        return self.name


class Break:
    def __init__(self, start_time, end_time):
        self.start_time = start_time
        self.end_time = end_time
        self.deltatime = end_time - start_time


def main():
    conferenses = []
    print('Программа для планирования конференций.')
    print('Команды:')
    print('1.Создать новую конференцию.')
    print('2.Выйти.')
    if input('Введите команду:') == '1':
        conferenses.append(Conference(input('Введите название конференции:')))
        print('Создана новая конференция.')

    while True:
        print('\n', '\n')
        print('Команды:')
        print('1.Создать новую конференцию.')
        print('2.Показать список конференций.')
        print('3.Выйти.')
        ans = input('Введите команду:')
        print('\n', '\n')
        if ans == '1':
            conferenses.append(Conference(input('Введите название конференции:')))
            print('Создана новая конференция.')
        elif ans == '2':
            if len(conferenses) == 0:
                print('Конференций не запланировано.')
            else:
                print('Существующие конференции:')
                for i, conferens in enumerate(conferenses, 1):
                    print(f'{i}.{str(conferens)}')
                print()
                print('Команды:')
                print('1.Открыть конференцию.')
                print('2.Выйти в меню.')
                ans = input('Введите команду:')
                print('\n', '\n')

                if ans == '1':
                    print('Существующие конференции:')
                    for i, conferens in enumerate(conferenses, 1):
                        print(f'{i}.{str(conferens)}')
                    print('\n', '\n')
                    conferens = conferenses[int(input('Номер конференции:')) - 1]
                    print()
                    while True:
                        print('Команды:')
                        print('1.Показать информацию о конференции.')
                        print('2.Добавить доклад.')
                        print('3.Выйти в меню.')
                        print()
                        ans = input('Введите команду:')
                        print('\n', '\n')
                        if ans == '1':
                            if len(conferens.conference) == 0:
                                print('В конференции нет не одного доклада.')
                            else:
                                print(conferens.print_info())
                        elif ans == '2':
                            conferens.add_report(Report())
                        else:
                            break
                else:
                    continue

        else:
            if input('Вы точно хотите выйти?(Y/N):') == 'Y':
                break


if __name__ == '__main__':
    main()