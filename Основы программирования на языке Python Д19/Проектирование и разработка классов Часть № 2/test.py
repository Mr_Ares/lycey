import datetime as dt

t1 = dt.timedelta(minutes=40)
t2 = dt.timedelta(minutes=10)

print((t1 - t2).total_seconds().__divmod__(60))  # количество минут и секунд
