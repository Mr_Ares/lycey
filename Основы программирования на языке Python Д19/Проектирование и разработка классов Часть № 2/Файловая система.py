class Directory:
    def __init__(self, name, mother_directory, path):
        self.name = name
        self.directory = {}
        self.mother_directory = mother_directory
        self.path = path

    def create_dir(self, ls_dir):  # Создание поддиректории
        if bool(ls_dir):
            dir_name = ls_dir.pop(0)
            if dir_name not in self.directory.keys():
                self.directory[dir_name] = Directory(dir_name, self, self.path + '/' + dir_name)
            self.directory[dir_name].create_dir(ls_dir)

    def __str__(self):
        return self.name

    def get_dir(self, ls_dir):
        if bool(ls_dir):
            dir_name = ls_dir.pop(0)
            if dir_name == '..':
                if self.mother_directory is None:
                    print('ОШИБКА В ПУТИ!!!')
                    return self
                return self.mother_directory.get_dir(ls_dir)
            if dir_name not in self.directory.keys():
                print(f'Директории {dir_name}, не существует!')
            return self.directory[dir_name].get_dir(ls_dir)
        else:
            return self

    def print_dir(self):
        print('Содержание директории:')
        for num, key in enumerate(self.directory.keys(), 1):
            if type(self.directory[key]) == Directory:
                tp = 'DIR'
            else:
                tp = 'FILE'
            print(f'{num}.{key}                  {tp}')
        print('\n \n')

    def create_file(self, name):
        self.directory[str(name)] = File(name, input('Содержимое файла:'))

    def get_file(self, file_name):
        return self.directory[file_name]


class File:
    def __init__(self, name, text):
        self.name = name
        self.text = text

    def __str__(self):
        return self.name

    def print_text(self):
        print(self.text)


def main():
    current_directory = Directory('Main', None, 'Main')
    print('md Directory                 -- создать директорию Directory',
          'cd Directory                 -- перейти в диреткорию Directory',
          'dir                          -- показать содержимое диреткории Directory',
          'copy con FileName.txt        -- создать файл в выбраной директории и записать текст',
          'FileName.txt                 -- прочитать содержание файла в выбраной директории',
          '', '', sep='\n')
    while True:
        command = input(current_directory.path + '>').split()
        if bool(command):

            if command[0] == 'md':
                directory = command[1].split('/')
                if type(directory) == str:
                    directory = [directory]
                current_directory.create_dir(directory[:])
                current_directory = current_directory.get_dir(directory[:])

            elif command[0] == 'cd':
                directory = command[1].split('/')
                if type(directory) == str:
                    directory = [directory]
                current_directory = current_directory.get_dir(directory[:])

            elif command[0] == 'dir':
                current_directory.print_dir()

            elif command[0] == 'copy':
                current_directory.create_file(command[2])

            else:
                if command[0] in list(map(lambda x: x.name, current_directory.directory.values())) \
                        and type(current_directory.directory[command[0]]) == File:
                    current_directory.directory[command[0]].print_text()
                else:
                    print('Неизвестная команда.')


if __name__ == '__main__':
    main()
